package main

import (
	"time"

	"github.com/go-redis/redis"
)

type Redis struct {
	client *redis.Client
}

func NewRedisClient(opt *redis.Options) (*Redis, error) {
	r := redis.NewClient(opt)
	_, err := r.Ping().Result()
	if err != nil {
		return nil, err
	}
	return &Redis{r}, nil
}

func (r *Redis) Set(key string, v string, t time.Duration) (bool, error) {
	err := r.client.Set(key, v, t).Err()
	if err != nil {
		return false, err
	}
	return true, nil
}

func (r *Redis) Get(key string) (bool, string) {
	val, err := r.client.Get(key).Result()
	if err == redis.Nil {
		return false, "key not found"
	} else if err != nil {
		return false, err.Error()
	} else {
		return true, val
	}
}

func (r *Redis) Scan(pattern string) (int, []string) {
	var cursor uint64
	var keys []string
	var n int
	for {
		var err error
		keys, cursor, err = r.client.Scan(cursor, pattern, 10).Result()
		if err != nil {
			panic(err)
		}
		n += len(keys)
		if cursor == 0 {
			break
		}
	}
	return n, keys
}
