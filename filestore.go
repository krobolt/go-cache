package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"
)

type Filestore struct {
	store    map[string]fsitem
	duration time.Duration
	location string
	tick     chan time.Time
	remove   chan fsitem
	close    chan int
}

type keystore struct {
	Key   string `json:"key"`
	Value string `json:"value"`
}

type fsitem struct {
	filename string
	expires  time.Duration
	created  int64
}

func NewFilestore(location string) *Filestore {
	filestore := &Filestore{
		store:    make(map[string]fsitem),
		duration: time.Minute * 5,
		location: location,
		tick:     make(chan time.Time),
		remove:   make(chan fsitem),
		close:    make(chan int),
	}
	go filestore.FilestoreEvents()
	go filestore.Tick()
	return filestore
}

func (fs *Filestore) Tick() {
	for {
		fs.tick <- time.Now()
		time.Sleep(fs.duration)
	}
}

//FilestoreEvents collects list of file to be removed from filestore every x mins
func (fs *Filestore) FilestoreEvents() {
	for {
		select {
		case t := <-fs.tick:
			for key, item := range fs.store {
				expired := t.Add(-item.expires).Unix()
				if expired-item.created > 0 {
					err := os.Remove(item.filename)
					if err != nil {
						log.Println(err)
					}
					delete(fs.store, key)
					log.Println("removed expired item", key)
				}
			}
		case <-fs.close:
			log.Println("closing")
			for key := range fs.store {
				log.Println("removed key")
				delete(fs.store, key)
			}
			return
		}
	}
}

func (f *Filestore) _Open(key string) (*os.File, error) {
	return os.OpenFile(f.location+":"+key, os.O_RDWR|os.O_CREATE, 0755)
}

func (f *Filestore) _Read(key string) ([]byte, error) {
	return ioutil.ReadFile(f.location + ":" + key)
}

func (f *Filestore) _Remove(key string) error {
	return os.Remove(f.location + ":" + key)
}

func (f *Filestore) Set(key string, v string, t time.Duration) (bool, error) {
	store := &keystore{Key: key, Value: v}
	b, err := json.Marshal(store)
	if err != nil {
		return false, err
	}
	err = f._Remove(key)
	if err != nil {
		fmt.Println(err)
	}
	file, _ := f._Open(key)
	defer file.Close()
	file.Write(b)
	name := f.location + ":" + key
	f.store[key] = fsitem{
		filename: name,
		expires:  t,
		created:  time.Now().Unix(),
	}
	return true, nil
}

func (f *Filestore) Get(key string) (bool, string) {
	result := keystore{}
	if _, ok := f.store[key]; ok {
		b, err := f._Read(key)
		if err != nil {
			return false, ""
		}
		if err := json.Unmarshal(b, &result); err != nil {
			return false, ""
		}
		return true, result.Value
	}
	return false, ""
}
