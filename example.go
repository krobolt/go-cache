package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"time"
)

func main() {
	//runs in background
	filestore := NewFilestore("./store")

	filestore.Set("key", "value", time.Second*10)
	filestore.Set("key2", "anothervalue", time.Second*20)
	filestore.Set("key", "shouldclear", time.Second*30)

	fmt.Println(filestore.Get("key"))
	fmt.Println(filestore.Get("key2"))

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*15)
	defer cancel()

	filestore.Shutdown(ctx)
	os.Exit(0)
}

func (f *Filestore) Shutdown(ctx context.Context) {

	f.close <- 1

}
