package main

import (
	"time"
)

type CacheInterface interface {
	Set(key string, v string, t time.Duration) (bool, error)
	Get(key string) (bool, string)
	Scan(pattern string) (int, []string)
}

type Cache struct{}
